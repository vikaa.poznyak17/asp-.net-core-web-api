﻿using API.BusinessLogic;
using API.Context;
using API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly UserOperations _operations;

        public ValuesController(MyContext context)
        {
            _operations = new(context);
        }

        [HttpGet]
        [Route("CreateAPI")]
        public ActionResult Create(string userName, string login, string password)
        {
            Users newUser = new()
            {
                UserName = userName,
                Login = login,
                Password = password
            };

            _operations.Create(newUser);

            return Ok();
        }

        [HttpPost]
        [Route("InsertAPI")]
        public ActionResult<Users> Insert(int Id)
        {
            Users user = _operations.Insert(Id);

            return user;
        }

        [HttpGet]
        [Route("UpdateAPI")]
        public ActionResult Update(int Id, string password)
        {
            _operations.Update(Id, password);

            return Ok();
        }

        [HttpGet]
        [Route("DeleteAPI")]
        public ActionResult Delete(int Id)
        {
            _operations.Delete(Id);

            return Ok();
        }
    }
}

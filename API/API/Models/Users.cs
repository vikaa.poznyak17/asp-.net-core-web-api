﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Users
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(25,MinimumLength = 3)]
        public string UserName { get; set; }

        [Required]
        [StringLength(25, MinimumLength = 10)]
        public string Login { get; set; }

        [Required]
        [StringLength(25, MinimumLength = 10)]
        public string Password { get; set; }

    }
}

﻿using API.Context;
using API.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.BusinessLogic
{
    public class UserOperations
    {
        private readonly MyContext _context;

        public UserOperations(MyContext context)
        {
            _context = context;
        }

        public void Create(Users user)
        {
            _context.User.Add(user);
            _context.SaveChanges();
        }

        public Users Insert(int Id)
        {
            Users user = _context.User.Find(Id);

            return user;
        }

        public void Update(int Id, string password)
        {
            Users user = _context.User.Find(Id);

            user.Password = password;
            _context.SaveChanges();
        }

        public void Delete(int Id)
        {
            Users user = _context.User.Find(Id);
            _context.User.Remove(user);

            _context.SaveChanges();
        }
    }
}
